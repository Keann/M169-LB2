######################################################################################
##  ACHTUNG: DIESES SCRIPT LÖSCHT ALLE INHALTE WELCHE DURCH DAS "deploy.sh"         ##
##  ERSTELLT WURDEN!                                                                ##
######################################################################################
##  Geschrieben wurde dieses Script durch Kean Mala, PE21b für die LB2 des Modules  ##
##  169 für die Technische Berufsschule  in Zürich.                                 ##
######################################################################################

## Alle Container killen
docker kill apache2-webserver
docker kill swag-reverseproxy

## Alle Container entfernen
docker rm apache2-webserver
docker rm swag-reverseproxy

## Container-Netzwerk entfernen
docker network rm webserver-network

## Geklontes Repo löschen
cd ..
rm -rf M169-LB2
cd ..
