######################################################################################
##  Dieses Script wird ausgeführt um eine Docker-Umgebung zu erstellen,             ##
##  welches einen Webserver mit Reverse-Proxy und gültigem SSL-Zertifikat haust.    ##
######################################################################################
##  Geschrieben wurde dieses Script durch Kean Mala, PE21b für die LB2 des Modules  ##
##  169 für die Technische Berufsschule  in Zürich.                                 ##
######################################################################################

## shabang zur Ausführung dieses Scripts im Unix Standard Bash 
#!/bin/bash

## Erstellung der Ordnerstruktur für die persitenten Volumen
mkdir apache2 rproxy-conf
echo "Basis-Ordnerstruktur erstellt"

## Erstellung des Docker-Network mit dem Namen "webserver-network" in welchem sich die Container befinden werden:
docker network create \
--driver=bridge \
--subnet=10.16.17.0/29 \
--gateway=10.16.17.1 \
--attachable \
webserver-network
echo "Docker-Netzwerk für Conatiner erstellt"

## Ausführung des docker-compose.yml File für die Erstellung der Container:
cd dockercompose
docker-compose up -d
cd ..
echo "Container erfolgreich gestartet"

## Hinzufügen des Webserver Container in das oben erstellte Docker-Network:
docker network connect \
--ip=10.16.17.2 \
webserver-network \
apache2-webserver
echo "Webserver-Container erfolgreich ins Netzwerk eingebunden"

##Hinzufügendes Reverse-Proxy Container in das oben erstelle Docker-Network:
docker network connect \
--ip=10.16.17.3 \
webserver-network \
swag-reverseproxy
echo "Reverse-Proxy Continer erfolgreich ins Netzwerk eingebunden"

## Reverse-Proxy neustarten
docker restart swag-reverseproxy

## Swag-Config in richtigen Pfad kopieren
docker exec --privileged -d swag-reverseproxy cp /docker/apache2.subdomain.conf /config/nginx/proxy-confs/

## Reverse-Proxy neustarten und Meldung anzeigen:
docker restart swag-reverseproxy
echo "Swag-Config erfolgreich übernommen"
docker restart apache2-webserver

## Meldung anzeigen dass Prozess vorbei ist:
echo "Alles Bereit! Webseite ereichbar über: m169-lb.deinedomain.ch"
